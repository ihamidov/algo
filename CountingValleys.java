public class CountingValleys {
    public static void main(String[] args) {
        System.out.println(countingValleys(8, "UDDDUDUU"));
    }

    public static int countingValleys(int steps, String path) {
        // Write your code here
        int level = 0;
        int valleys = 0;
        for (char c : path.toCharArray()) {
            if (c =='U'){
                ++level;
                if (level==0){
                    ++valleys;
                }
            } else {
                --level;
            }
        }
        return valleys;
    }
}
