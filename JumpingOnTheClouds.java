import java.util.List;

public class JumpingOnTheClouds {
    public static void main(String[] args) {
        System.out.println(jumpingOnClouds(List.of(0, 0, 0, 1, 0, 0)));//3
    }

    public static int jumpingOnClouds(List<Integer> c) {
        // Write your code here
        int count = 0;
        for (int i = 0; i < c.size()-1; ) {
            if (i + 2 < c.size() && c.get(i + 2) != 1) {
                ++count;
                i += 2;

            } else {
                ++count;++i;
            }
        }
        return count;
    }
}
