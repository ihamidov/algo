import java.math.BigInteger;

public class Factorial {
    public static void main(String[] args) {
        extraLongFactorials(25);
    }

    public static void extraLongFactorials(int n) {
        // Write your code here
        BigInteger num = BigInteger.ONE;
        while (n != 0){
            num = num.multiply(BigInteger.valueOf(n));
            n -= 1;
        }
        System.out.println(num);

    }
}
