public class CloseToZero {
    public static void main(String[] args) {
        int[] ints = {-9, 8, 2, -5, 7, -1};
        System.out.println(closeToZero(ints));
    }


    private static int closeToZero(int[] ints) {
        int closerToZero = Math.abs(ints[0]);
        int result = 0;
        for (int i = 1; i < ints.length; i++) {
            if (closerToZero > Math.abs(ints[i])) {
                closerToZero = Math.abs(ints[i]);
                result = ints[i];
            }
        }
        return result;
    }


}
