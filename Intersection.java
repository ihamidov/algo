public class Intersection {
    public static void main(String[] args) {
        System.out.println("son " + test3(471, 480));
    }

    static int getDigitsSum(int x) {
        int sum = 0;
        while (x != 0) {

            sum += x % 10;
            x /= 10;
        }

        return sum;
    }

    static int test3(int a, int b) {
        while (a != b) {
            a = a + getDigitsSum(a);
            b = b + getDigitsSum(b);
            System.out.println(a + " " + b);
        }
        return a;
    }
}
