import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public class Ratio {
    public static void main(String[] args) {
        BigDecimal s = BigDecimal.valueOf(0.00343125); //new BigDecimal("0.00343125");
        BigDecimal e = BigDecimal.valueOf(0.34312499);//new BigDecimal("0.34312499");
        var ratioBigdecimal = s.divide(e, 8, RoundingMode.DOWN).setScale(0,RoundingMode.HALF_UP);
        System.out.println(ratioBigdecimal);
        System.out.println(convertToFraction(ratioBigdecimal));
//        System.out.println(convertToFraction(BigDecimal.valueOf(999.99221)));
    }

    public static String convertToFraction(BigDecimal decimal) {
        BigInteger numerator = decimal.multiply(BigDecimal.valueOf(100)).toBigInteger();
        BigInteger denominator = BigInteger.valueOf(100);
        BigInteger gcd = numerator.gcd(denominator);
        numerator = numerator.divide(gcd);
        denominator = denominator.divide(gcd);
        return numerator + ":" + denominator;
    }
}
