public class RepeatedString {
    public static void main(String[] args) {
        System.out.println(repeatedString("a", 1000000));
    }

    public static long repeatedString(String s, long n) {
        // Write your code here
        char [] c= s.toCharArray();
        long sCount = 0;
        for (char d : c) {
            if (d=='a') {
                ++sCount;
            }
        }
        long whole = n/(long)s.length();
        long wholeSideCount = whole*sCount;
        System.out.println("wholeSideCount "+wholeSideCount);

        long fractional = n%(long)s.length();
        System.out.println("fractional "+fractional);
        String fractionalString = s.substring(0,(int)fractional);
        System.out.println("fractionalString "+fractionalString);

        long fCount = 0;

        for (char d : fractionalString.toCharArray()) {
            if (d=='a') {
                ++fCount;
            }
        }
        return wholeSideCount+fCount;
    }

}

