public class IntegerConvertion {
    public static void main(String[] args) {
        int a = 12345;
        int reversed = reverseInteger(a);
        System.out.println("Reversed integer: " + reversed);
    }

    public static int reverseInteger(int number) {
        int reversed = 0;
        while (number != 0) {
            reversed = reversed * 10 + number % 10;
            number /= 10;
        }
        return reversed;
    }
}