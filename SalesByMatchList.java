import java.util.List;

public class SalesByMatchList {
    public static void main(String[] args) {
        System.out.println(sockMerchant(20, List.of(4, 5, 5, 5, 6, 6, 4, 1, 4, 4, 3, 6, 6, 3, 6, 1, 4, 5, 5, 5)));
    }

    public static int sockMerchant(int n, List<Integer> ar) {
        final int MAX = 101;
        int[] count = new int[MAX];
        for (Integer el : ar) {
            count[el]++;
        }
        int result = 0;
        for (int i = 0; i < MAX; i++) {
            result += count[i] / 2;
        }
        return result;

    }
}
