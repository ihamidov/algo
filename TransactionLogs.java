import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TransactionLogs {
    public static void main(String[] args) {
        processLogs(List.of("1 2 50", "1 7 20", "1 3 20", "2 2 17"), 2);

    }

    public static List<String> processLogs(List<String> logs, int threshold) {
        HashMap<String, Integer> map = new HashMap<>();
        for (String log : logs) {
            var element = log.split(" ");
            if (!element[0].equals(element[1]) && !map.containsKey(element[0]) && !map.containsKey(element[1])) {
                map.put(element[0], 1);
                map.put(element[1], 1);
            } else if (map.containsKey(element[0]) && !map.containsKey(element[1])) {
                var existElement = map.get(element[0]);
                map.put(element[0], existElement + 1);
                map.put(element[1], 1);
            } else if (!map.containsKey(element[0]) && map.containsKey(element[1])) {
                var existElement = map.get(element[1]);
                map.put(element[0], 1);
                map.put(element[1], existElement + 1);
            }

            if (element[0].equals(element[1]) && !map.containsKey(element[0])) {
                map.put(element[0], 1);
            } else if (element[0].equals(element[1]) && map.containsKey(element[0])) {
                var existElement = map.get(element[0]);
                map.put(element[1], existElement + 1);
            }
        }

        List<String> collect = new ArrayList<>();
        map.entrySet().stream().filter(i -> i.getValue() >= threshold).forEach(i->{
            collect.add(i.getKey().toString());
        });
        System.out.println(collect);

//        var collect = map.entrySet().stream()
//                .filter(i -> i.getValue() >= threshold).map(i->{
//                    return List.of(i.getValue().toString());
//                }).flatMap(List::stream).toList();
//        System.out.println(collect); // Java8+
        System.out.println(map);

        return null;
    }

}
