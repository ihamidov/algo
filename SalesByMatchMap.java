import java.util.HashMap;
import java.util.List;

public class SalesByMatchMap {
    public static void main(String[] args) {
        System.out.println(sockMerchant(20, List.of(4 ,5 ,5 ,5, 6, 6, 4, 1, 4, 4, 3, 6, 6, 3, 6, 1, 4, 5, 5, 5)));
    }

    public static int sockMerchant(int n, List<Integer> ar) {
        HashMap<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < n; ++i) {
            if (map.containsKey(ar.get(i))) {
                map.put(ar.get(i), map.get(ar.get(i)) + 1);
            } else {
                map.put(ar.get(i), 1);
            }
        }
        System.out.println("map "+map);
        return map.entrySet().stream()
                .filter(i -> i.getValue() >= 2)
                .map(i -> i.getValue() / 2).reduce(0, Integer::sum);


    }
}
